package ru.example.hibernate.springdatajpa;

import org.springframework.data.jpa.repository.Lock;
import ru.example.hibernate.springdatajpa.entities.Singer;

import javax.persistence.LockModeType;
import java.util.List;

public interface SingerService {
    Singer findById(Long id);
    List<Singer> findAll();
    List<Singer> findByFirstName(String firstName);
    List<Singer> findByFirstNameAndLastName(String firstName, String lastName);
}
