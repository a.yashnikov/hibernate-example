package ru.example.hibernate.transactions_prog.services;

import ru.example.hibernate.entities.Singer;

import java.util.List;

public interface SingerService {
    List<Singer> findAll();
    Singer findById(Long id);
    Singer save(Singer singer);
    long countAll();
}
