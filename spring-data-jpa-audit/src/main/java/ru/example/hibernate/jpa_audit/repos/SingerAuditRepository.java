package ru.example.hibernate.jpa_audit.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.example.hibernate.jpa_audit.entities.SingerAudit;
import org.springframework.data.repository.CrudRepository;

public interface SingerAuditRepository extends JpaRepository<SingerAudit, Long> {
}
