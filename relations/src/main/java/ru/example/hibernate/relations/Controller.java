package ru.example.hibernate.relations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.example.hibernate.relations.service.ParsedQRService;

@RestController
public class Controller {
    private final ParsedQRService parsedQRService;

    @Autowired
    public Controller(ParsedQRService parsedQRService) {
        this.parsedQRService = parsedQRService;
    }

    @GetMapping(path = "/transactionRollback")
    public String showTransactionRollback() {
        parsedQRService.changeDataRollback();
        return "ok";
    }

    @GetMapping(path = "/pessimisticLock")
    public String showPessimisticLock() {
        parsedQRService.changeData();
        return "ok";
    }
}
